#ifndef FILTER_H
#define FILTER_H


#include "constants.h"

#define ANTHILL_3

// Coutinho: If a c++ filter use this file, tell it that these functions are in C
#ifdef __cplusplus
extern "C" {
#endif

#define EOW -2 ///< the end of work message code
#define ERROR -3 ///< Reader functions should return this on a internal error

///< Possible fault status
#define NO_FAULT -1 ///< No fault has occurred until now
#define FAULT_OTHER_FILTER_INST -2 ///< One fault has occurred in other filter instance

typedef int RecoverCallback_t( int taskId, int *deps, int depSize, char *metadata, int metaSize );

#ifndef _PORT_HANDLERS_
#define _PORT_HANDLERS_
typedef int InputPortHandler, OutputPortHandler;
#endif

/*! \file FilterDev.h These functions all use the FilterData *fd global variable,
 * which represents the filter on the client side. These functions are all available
 * to the filter developer.
 *
 * Global variable fd is declared on FilterData.h
 */

//the filter library functions datatypes
typedef int initialize_t(void *work, int worksize);
typedef int process_t(void *work, int worksize);
typedef int finalize_t();

initialize_t initFilter;
process_t processFilter;
finalize_t finalizeFilter;

/// get the handlers of ports
InputPortHandler ahGetInputPortByName(char *name);
OutputPortHandler ahGetOutputPortByName(char *name);

OutputPortHandler ahGetOutputPortByNameSameHost(char *name);

int ahCloseOutputPort(OutputPortHandler oph); //< close an output port, sending EOW to receivers

/// funtion to probe an input port for data, returns the size of the buffer, 0 otherwise
int ahProbe(InputPortHandler ip);
/// function that reads from other filter(receive and unpack)
int ahReadBuffer(InputPortHandler ip, void *buf, int szBuf);
/// returns 0 if there is no data to be received
int ahReadNonBlockingBuffer(InputPortHandler iph, void *buf, int szBuf);

/// returns 0 if there is no data to be received
int ahReadNonBlockingBufferMalloc(InputPortHandler iph, void **bufOut, char **labelOut);

/// this function reads a buffer, of size szBuf, of any port
/// The exceptionPorts are the ports that the Anthill doesn't need to wait the EOWs
int ahReadBufferAnyPort(char **portName, char **exceptionPorts, int numExc, void *buf, int szBuf);
/// function that writes to other filter(pack and send)
int ahWriteBuffer(OutputPortHandler op, void *buf, int bufSz);

/** these functions return the number of instances on the other side of the strem
  * say, we have a filter A->B. B knows it will receive one message only from each A. He can use this number
  * to leave a loop like while (numMessages != ahGetNumWriters(iph))
  */
char **ahGetArgv();
int ahGetArgc();

int ahGetNumWriters(InputPortHandler iph);
// Get the count of instances that read from the referred port. This number is
// the sum of instances of each filter that read from the referred port.
int ahGetNumReaders(OutputPortHandler oph);

/// function to start packing data, size in bytes
int ahInitPack(int initSize);

/// function to pack data to the buffer
int ahPackData(void *data, int size);
/// function to send the packed buffer
int ahWritePackedBuffer(OutputPortHandler oph);

/// function to receive a buffer, and unpack later
int ahInitReceive(InputPortHandler iph);
/// function to unpack data
int ahUnpackData(void *buf, int size);

/// get filter name
char *ahGetFilterName();
/// get filter id
int ahGetFilterId();

/// function to get the number of one filter input ports
int ahGetNumInputPorts();
/// function to get all input ports names of one filter
char **ahGetInputPortNames();
/// function to get the number of one filter output ports
int ahGetNumOutputPorts();
/// function to get all output ports names of one filter
char **ahGetOutputPortNames();

/// get the number of writers to me that are still running
int ahGetNumUpStreamsRunning();

/// get the ammount of memory this machine has for this execution
int ahGetMachineMemory();
/// the number of brothers I have in this machine + 1
/// brothers are the same filters as me, so say, you have a filter A
/// running here, and 2 Bs runnning here, if A calls this hell get 1,
/// and B will get 2.
int ahGetLocalInstances();

/// Function that returns which brother am I
int ahGetMyRank();
/// returns the total intances of this filter(me + mybrothers)
int ahGetTotalInstances();
/// kill all filters and finish void, user calls this to exit the system abnormally
int ahExit(char *mesg);

/** Task functions */
void ahUseTasks();
/// creates a new task
int ahCreateTask(int taskId, int *deps, int depSize, char *metadata, int metaSize);
/// ends a task
int ahEndTask(int taskId);
///get the current task we are working on
int ahGetCurrentTask();
int ahSetCurrentTask(int taskId);
int *ahGetTaskDeps(int taskId, int *depsSz);
int ahGetRunningTasks( int * numTasks, int ** taskList );
int *ahGetFinishedTasks(int *numTasks);

int ahRegisterRecoverCallback( RecoverCallback_t * callback );

// data functions
int ahPutData(char *id, void *val, int valSize);
int ahRemoveData(char *id);
void *ahGetData(int taskId, char *id, int *valSz);

// instrumentation stuff
void ahInstSetStates(char **states, int numStates);
void ahInstSwitchState(int stateId);
void ahInstEnterState(int stateId);
void ahInstLeaveState();

#ifdef BMI_FT
void ahInstSaveIntermediaryState();
#endif

/// Returns the status of the fault
int ahAdvertiseFault();

/// Returns 1 if the filter is the last one in the pipeline or 0 otherwise
int ahLastFilter();

#ifdef __cplusplus
}
#endif

#endif
