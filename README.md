Anthill
=======
Antihill is a framework that supports the development of parallel applications.

Anthill provides two main abstractions: streams and filters, that the developer
can combine to build complex systems. Currently Anthill contains two API's: one
in C, and another in C++. Anthill may be configured to run in a variety of
clusters, from grids of commodity processors to highly parallel graphical
processing units.

Installing
=======

In order to install Anthill on Ubuntu (tested on 12.04.5 64 Bits) you will need
to run this command:

```Bash
sudo apt-get install build-essential git pvm-dev libexpat1-dev
```

Then, clone this repository:

```Bash
git clone https://github.com/michelboaventura/anthill.git
```

And compile and install it:

```Bash
cd anthill
make
sudo make install
```

Linking
=======

To compile and link an app using Anthill, you have do include
/usr/include/anthill:

```Makefile
 -I/usr/include/anthill
```

and link it with libah and libahevent:

```Makefile
  -lah -lahevent
```

An example would be:

```Bash
  g++ -fPIC -Wall -I/usr/include/anthill -lah -lpvm3 -lexpat -lahevent -shared Reader.cpp -o Reader.so
```
